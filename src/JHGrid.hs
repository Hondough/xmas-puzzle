module Grid where

import Data.Vector as V
import Data.List as L

data Block = B | W | U

data Line = Line {
  index :: Int,
  lDir :: Dir,
  blackRuns :: [Int],
  line :: Vector Block
} deriving (Show)

data Dir = Row | Col deriving (Eq, Show)

type Grid = Vector Block

instance Show Block where
  show B = "B"
  show W = "W"
  show U = "?"

instance Eq Block where
  B == B = True
  W == W = True
  U == U = True
  _ == _ = False

softEQ :: Block -> Block -> Bool
softEQ U _ = True
softEQ _ U = True
softEQ b b' = b == b'

instance Ord Block where
  compare B W = GT
  compare W B = LT
  compare _ _ = EQ

row :: Int -> Int
row = flip div 25

col :: Int -> Int
col = flip mod 25

rowCol :: Int -> (Int, Int)
rowCol i = (row i, col i)

gridIndex :: Line -> [Int]
gridIndex l
  | lDir l == Row = L.map (\x -> index l * 25 + x) [0..24]
  | otherwise = L.map (\x -> x * 25 + index l) [0..24]

blockCheck :: Block -> Block -> Bool
blockCheck _ U = True
blockCheck U _ = True
blockCheck inBlock gridBlock = inBlock == gridBlock

consistent :: Vector Block -> Vector Block -> Bool
consistent l l' = V.and $ V.zipWith blockCheck l l'

blackStart :: [(Int, Int)]
blackStart = [(3,3), (3,4), (3,12), (3,13), (3,21)
  , (8,6), (8,7), (8,10), (8,14), (8,15), (8,18)
  , (16,6), (16,11), (16,16), (16,20)
  , (21,3), (21,4), (21,9), (21,10), (21,15), (21,20), (21,21)]

blankGrid :: Grid
blankGrid = V.replicate (25*25) U

writeGridB :: [(Int, Int)] -> Grid -> Grid
writeGridB coords = flip (V.//) $ indexesB $ gridIndexes coords

startGrid :: Grid
startGrid = writeGridB blackStart blankGrid

gridIndexes :: [(Int, Int)] -> [Int]
gridIndexes = Prelude.map $ \(r, c) -> r * 25 + c

indexesB :: [Int] -> [(Int, Block)]
indexesB = flip Prelude.zip $ repeat B

makeLine :: Int -> Dir -> [Int] -> Line
makeLine i d is = Line i d is V.empty

expand :: [Int] -> Vector Block
expand [w] = runs w W
expand (w:b:line) = runs w W V.++ runs b B V.++ expand line

runs :: Int -> Block -> Vector Block
runs = V.replicate

runsNew :: (Block, Int) -> Vector Block
runsNew (b, i) = runs i b

writeGridBlock :: Int -> Dir -> Vector Block -> Grid -> Grid
writeGridBlock = undefined

writeGridLine :: Line -> Grid -> Grid
writeGridLine l g = g V.// L.zip (gridIndex l) (V.toList (line l))

readGridBlock :: (Int, Int) -> Grid -> Block
readGridBlock (r, c) = flip (V.!) $ r * 25 + c

readGridLine :: Int -> Dir -> Grid -> Vector Block
readGridLine i Row g = V.generate 25 (\x -> readGridBlock (i, x) g)
readGridLine i Col g = V.generate 25 (\x -> readGridBlock (x, i) g)
