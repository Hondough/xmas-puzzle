module Main where

import Solver
import Grid
import Data.List
import Data.List.Extra (nubOrd)
import Data.Vector (Vector)
import qualified Data.Vector as V

-- http://www.gchq.gov.uk/press_and_media/news_and_features/Pages/Directors-Christmas-puzzle-2015.aspx

main :: IO ()
main = do
  print "Welcome to the X-mas Puzzle Solver"
--  print startGrid
