module Solver where

import Grid
import Data.List
import Data.Vector (Vector)
import qualified Data.Vector as V

rows :: [[Int]]
rows = [
  [7,3,1,1,7],
  [1,1,2,2,1,1],
  [1,3,1,3,1,1,3,1],
  [1,3,1,1,6,1,3,1],
  [1,3,1,5,2,1,3,1],
  [1,1,2,1,1],
  [7,1,1,1,1,1,7],
  [3,3],
  [1,2,3,1,1,3,1,1,2],
  [1,1,3,2,1,1],
  [4,1,4,2,1,2],
  [1,1,1,1,1,4,1,3],
  [2,1,1,1,2,5],
  [3,2,2,6,3,1],
  [1,9,1,1,2,1],
  [2,1,2,2,3,1],
  [3,1,1,1,1,5,1],
  [1,2,2,5],
  [7,1,2,1,1,1,3],
  [1,1,2,1,2,2,1],
  [1,3,1,4,5,1],
  [1,3,1,3,10,2],
  [1,3,1,1,6,6],
  [1,1,2,1,1,2],
  [7,2,1,2,5]
  ]

cols :: [[Int]]
cols = [
  [7,2,1,1,7],
  [1,1,2,2,1,1],
  [1,3,1,3,1,3,1,3,1],
  [1,3,1,1,5,1,3,1],
  [1,3,1,1,4,1,3,1],
  [1,1,1,2,1,1],
  [7,1,1,1,1,1,7],
  [1,1,3],
  [2,1,2,1,8,2,1],
  [2,2,1,2,1,1,1,2],
  [1,7,3,2,1],
  [1,2,3,1,1,1,1,1],
  [4,1,1,2,6],
  [3,3,1,1,1,3,1],
  [1,2,5,2,2],
  [2,2,1,1,1,1,1,2,1],
  [1,3,3,2,1,8,1],
  [6,2,1],
  [7,1,4,1,1,3],
  [1,1,1,1,4],
  [1,3,1,3,7,1],
  [1,3,1,1,1,2,1,1,4],
  [1,3,1,4,3,3],
  [1,1,2,2,2,6,1],
  [7,1,3,2,1,1]]

freeSpace :: [Int] -> Int
freeSpace l = 25 - sum l - (length l - 1)

-- returns all possible permutations of l things taken fs at a time where l
-- things add up to l
combo :: Int -> Int -> [[Int]]
combo fs 1 = [[fs]]
combo 0 l = [replicate l 0]
combo fs l = [ x:cs |  x <- [0 .. fs]
                    , cs <- combo (fs-x) (l-1)
                          ]

-- gives the White possibilities for a black run
baseWhites black = combo fs l
  where
    fs = freeSpace black
    l = length black + 1

-- corrects the white possibilities by 1 in the middle gaps (because there is
-- a minimum of 1 white between black runs)
whites :: [[Int]] -> [[Int]]
whites = map minWhite
  where
    minWhite (w:ws) = head : map (1+) body ++ last
      where
        l = length ws
        head = w
        body = take (l - 1) ws
        last = drop (l - 1) ws

-- takes a line of the grid and list of Int (which define black runs)
-- determines all possibilities of runs that can be put in the grid line,
-- based on what is already there. It then places whites and blacks
-- where they must be. Unknowns are padded in when it isn't definitely a white or black
definiteChoices :: Vector Block -> [Int] -> [(Block, Int)]
definiteChoices gl black = foldr1 partialApply choices
  where
    choices = filter filterConsistent $ map (joinLine black) $ whites $ baseWhites black
    joinLine _ [w] = [(W, w) | w > 0]
    joinLine (b:bs) (w:ws) = [(W, w) | w > 0] ++ [(B, b)] ++ joinLine bs ws
    filterConsistent l = consistent gl $ runToLine l

test0 = definiteChoices (readGridLine 1 Row startGrid) r
  where
    r = blackRuns $ head $ drop 7 allData
test1 = definiteChoices (readGridLine 1 Row startGrid)  [3,3]
test2 = definiteChoices (readGridLine 3 Row startGrid)  [1,3,1,1,6,1,3,1]
test3 l = definiteChoices (readGridLine i d g) r
  where
    i = index l
    d = lDir l
    g = startGrid
    r = blackRuns l

foo g l = definiteChoices (readGridLine i d g) r
  where
    i = index l
    d = lDir l
    r = blackRuns l

bar g = foldl' (\acc y -> writeGridLine (thisLine y acc) acc) g allData
  where
    thisLine x a = Line (index x) (lDir x) (blackRuns x) (runToLine $ foo a x)

fooBar g
  | U `elem` g = fooBar $ bar g
  | otherwise = g


--converts a list of different block runs into a "line"
runToLine :: [(Block, Int)] -> Vector Block
runToLine = foldr ((V.++) . runsNew) V.empty

--combines two different block runs into one which is consistent with both
partialApply [] [] = []
partialApply ((b, l):bs) ((b', l'):bs')
  | b == b' && l == l' = (b, l)   : partialApply bs bs'
  | b == b' && l < l'  = (b, l)   : partialApply bs ((b', l' - l):bs')
  | b == b' && l > l'  = (b, l')  : partialApply ((b, l - l'):bs) bs'
  -- if it gets here, they are equal to each other, so slap in an Unknown
  |            l == l' = (U, l)   : partialApply bs bs'
  |            l < l'  = (U, l)   : partialApply bs ((b', l' - l):bs')
  |            l > l'  = (U, l')  : partialApply ((b, l - l'):bs) bs'
  | otherwise = []


allData = sortOn (freeSpace . Grid.blackRuns) $ rowsL ++ colsL
  where
    rowsL = zipWith (\i r -> makeLine i Row r) [0..] rows
    colsL = zipWith (\i c -> Line i Col c V.empty) [0..] cols

{-
[
B,B,B,B,B,B,B,W,B,B,B,W,W,W,B,W,B,W,B,B,B,B,B,B,B,
B,W,W,W,W,W,B,W,B,B,W,B,B,W,W,W,W,W,B,W,W,W,W,W,B,
B,W,B,B,B,W,B,W,W,W,W,W,B,B,B,W,B,W,B,W,B,B,B,W,B,
B,W,B,B,B,W,B,W,B,W,W,B,B,B,B,B,B,W,B,W,B,B,B,W,B,
B,W,B,B,B,W,B,W,W,B,B,B,B,B,W,B,B,W,B,W,B,B,B,W,B,
B,W,W,W,W,W,B,W,W,B,B,W,W,W,W,W,W,W,B,W,W,W,W,W,B,
B,B,B,B,B,B,B,W,B,W,B,W,B,W,B,W,B,W,B,B,B,B,B,B,B,
W,W,W,W,W,W,W,W,B,B,B,W,W,W,B,B,B,W,W,W,W,W,W,W,W,
B,W,B,B,W,B,B,B,W,W,B,W,B,W,B,B,B,W,B,W,W,B,W,B,B,
B,W,B,W,W,W,W,W,W,B,B,B,W,B,B,W,W,W,W,B,W,W,W,B,W,
W,B,B,B,B,W,B,W,B,B,B,B,W,B,B,W,B,W,W,W,W,B,B,W,W,
W,B,W,B,W,W,W,B,W,W,W,B,W,B,W,B,B,B,B,W,B,W,B,B,B,
W,W,B,B,W,W,B,W,B,W,B,W,W,W,W,W,W,B,B,W,B,B,B,B,B,
W,W,W,B,B,B,W,B,B,W,B,B,W,B,B,B,B,B,B,W,B,B,B,W,B,
B,W,B,B,B,B,B,B,B,B,B,W,B,W,B,W,W,B,B,W,W,W,W,B,W,
W,B,B,W,B,W,W,B,B,W,W,W,B,B,W,B,B,B,W,W,W,W,W,B,W,
B,B,B,W,B,W,B,W,B,W,W,B,W,W,W,W,B,B,B,B,B,W,B,W,W,
W,W,W,W,W,W,W,W,B,W,W,W,B,B,W,B,B,W,W,W,B,B,B,B,B,
B,B,B,B,B,B,B,W,B,W,W,B,B,W,W,W,B,W,B,W,B,W,B,B,B,
B,W,W,W,W,W,B,W,B,B,W,W,B,W,W,B,B,W,W,W,B,B,W,B,W,
B,W,B,B,B,W,B,W,W,W,B,B,B,B,W,W,B,B,B,B,B,W,W,B,W,
B,W,B,B,B,W,B,W,B,B,B,W,B,B,B,B,B,B,B,B,B,B,W,B,B,
B,W,B,B,B,W,B,W,B,W,W,B,B,B,B,B,B,W,B,B,B,B,B,B,W,
B,W,W,W,W,W,B,W,W,B,B,W,W,W,W,W,W,B,W,B,W,B,B,W,W,
B,B,B,B,B,B,B,W,B,B,W,W,W,B,W,B,B,W,W,W,B,B,B,B,B]

-}
